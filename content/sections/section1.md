---
title: "About"
weight: 2
section_id: "about"
---

I am a highly motivated individual with a unique blend of business administration, cybersecurity, and software development skills. With a strong educational background in both fields, including a Bachelor's degree in Management and a Master's degree in Cybersecurity, I possess a solid foundation in data analysis, problem-solving, and effective communication. Additionally, I am proficient in software development in Python, Rust, and JavaScript, enabling me to contribute to the development of secure and robust systems.

